﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGCData.Classes
{
    public class Flight
    {
        public int FlightId { get; set; }
        public string FlightName { get; set; }

        public int EventId{ get; set; }
        public Event Event { get; set; }

        public virtual ICollection<PilotLoggerFlight> PilotLoggerFlights { get; set; }


    }
}
