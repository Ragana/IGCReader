﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGCData.Classes
{
    public class Pilot
    {
        public int PilotId { get; set; }
        public string PilotName { get; set; }


        public virtual ICollection<PilotEventLogger> PilotsEventLoggers { get; set; }
        public virtual ICollection<PilotLoggerFlight> PilotLoggerFlights { get; set; }

    }
}
