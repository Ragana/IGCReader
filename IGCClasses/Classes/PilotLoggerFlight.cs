﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGCData.Classes
{
    public class PilotLoggerFlight
    {
        [Key, Column(Order = 0)]
        public int FlightId { get; set; }

        [Key, Column(Order = 1)]
        public int PilotId { get; set; }

        [Key, Column(Order = 2)]
        public int LoggerId { get; set; }

        [Key, Column(Order = 3)]
        public int TrackId { get; set; }

        public virtual Flight Flight { get; set; }
        public virtual Pilot Pilot { get; set; }
        public virtual Logger Logger { get; set; }
        public virtual Track Track { get; set; }


    }
}
