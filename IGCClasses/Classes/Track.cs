﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGCData.Classes
{
    public class Track
    {
        public int TrackId { get; set; }
        public string TrackName { get; set; }
        public DateTime? TrackDate { get; set; }
        public string GeodeticDatum { get; set; }
        public int AtmospherePress { get; set; }
        public int LoggingInterval { get; set; }
        public int AltitudeOffset { get; set; }
        public int UtcShift { get; set; }
        public string AltitudeType { get; set; }
        public string AltitudeUnit { get; set; }
        public string VerticalSpeedUnit { get; set; }
        public string SpeedUnit { get; set; }
        public string QnhUnit { get; set; }
        public bool MultiplemarkerDrop { get; set; }
        public int DeclarationDigits { get; set; }
        public decimal? InstrCalibration { get; set; }
        public decimal? PressOffset { get; set; }
        public string Header { get; set; }

        public string LoggerSerial { get; set; }

        public List<Location> Locations { get; set; }
        public List<Mark> Marks { get; set; }
        public List<Declaration> Declarations { get; set; }

        public virtual ICollection<PilotLoggerFlight> PilotLoggerFlights { get; set; }
    }
}
