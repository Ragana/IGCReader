﻿using System.Collections.Generic;

namespace IGCData.Classes
{
    public class Event
    {
        public int EventId { get; set; }
        public string EventName { get; set; }

        public List<Flight> Flights { get; set; }
     

        public virtual ICollection<PilotEventLogger> PilotEventLoggers { get; set; }
    }
}
