﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IGCData.Classes;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace IGCData.Classes
{
    public class PilotEventLogger
    {
        [Key, Column(Order = 0)]
        public int PilotId { get; set; }

        [Key, Column(Order = 1)]
        public int EventId { get; set; }

        [Key, Column(Order = 2)]
        public int LoggerId { get; set; }


        public virtual Pilot Pilot { get; set; }
        public virtual Event Event { get; set; }
        public virtual Logger Logger { get; set; }

        public int? PilotNumber { get; set; }
    }
}
