﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGCData.Classes
{

    public class Logger
    {
        public int LoggerId { get; set; }
        public string LoggerSerial { get; set; }

        public virtual ICollection<PilotEventLogger> PilotEventLoggers { get; set; }
    }
}
