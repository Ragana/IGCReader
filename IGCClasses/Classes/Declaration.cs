﻿using System;

namespace IGCData.Classes
{
    public class Declaration
    {
        public int DeclarationId { get; set; }
        public DateTime DeclarationTime { get; set; }
        public int DeclarationIdReceived{ get; set; }
        public string DeclaredGoalCoordinateDescr { get; set; }
        public string DeclaredGoalAltitudeDescr { get; set; }

        public int TrackId { get; set; }
        public Track Track { get; set; }

    }
}
