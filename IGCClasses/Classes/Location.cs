﻿using System;

namespace IGCData.Classes
{
    public class Location
    {
        public int LocationId { get; set; }
        public double Latitude { get; set; }
        public double Longtitude { get; set; }
        public DateTime LocationTime { get; set; }
        public int AltitudePress { get; set; }
        public int AltitudeGps { get; set; }
        public int VerticalSpeed { get; set; }
        public int TlcHdo { get; set; }
        public int TlcSiu { get; set; }

        public int TrackId { get; set; }
        public Track Track { get; set; }

    }
}
