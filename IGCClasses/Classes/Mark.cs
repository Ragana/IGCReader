﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGCData.Classes
{
    public class Mark
    {
        public int MarkId { get; set; }
        public int MarkerId { get; set; }
        public double LatitudeMark { get; set; }
        public double LongtitudeMark { get; set; }
        public DateTime MarkTime { get; set; }
        public int AltitudePressMark { get; set; }
        public int AltitudeGpsMark { get; set; }
        public int VerticalSpeedMark { get; set; }
        public int TlcHdoMark { get; set; }
        public int TlcSiuMark { get; set; }

        public int TrackId { get; set; }
        public Track Track { get; set; }
    }
}
