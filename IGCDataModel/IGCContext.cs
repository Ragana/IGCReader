﻿using IGCData.Classes;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;

namespace IGCData.Model
{
    public class IGCDataContext: DbContext 
    {
        public IGCDataContext()
        {
            Database.SetInitializer<DbContext>(new CreateDatabaseIfNotExists<DbContext>());
        }

        public DbSet<Event> Event { get; set; }
        public DbSet<Flight> Flight { get; set; }
        public DbSet<Pilot> Pilot {get; set; }
        public DbSet<Logger> Logger { get; set; }
        public DbSet<Track> Track {get; set; }
        public DbSet<Location> Location  {get; set; }
        public DbSet<Mark> Mark { get; set; }
        public DbSet<Declaration> Declaration { get; set; }
        public DbSet<PilotEventLogger> PilotEventLoggers { get; set; }
        public DbSet<PilotLoggerFlight> PilotLoggerFlights { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.Configurations.Add(new System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<EntityType>());
        //}

    }

}
