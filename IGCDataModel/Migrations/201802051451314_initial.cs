namespace IGCDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Declarations",
                c => new
                    {
                        DeclarationId = c.Int(nullable: false, identity: true),
                        DeclarationTime = c.DateTime(nullable: false),
                        DeclarationIdReceived = c.Int(nullable: false),
                        DeclaredGoalCoordinateDescr = c.String(),
                        DeclaredGoalAltitudeDescr = c.String(),
                        TrackId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DeclarationId)
                .ForeignKey("dbo.Tracks", t => t.TrackId, cascadeDelete: true)
                .Index(t => t.TrackId);
            
            CreateTable(
                "dbo.Tracks",
                c => new
                    {
                        TrackId = c.Int(nullable: false, identity: true),
                        TrackName = c.String(),
                        TrackDate = c.DateTime(),
                        GeodeticDatum = c.String(),
                        AtmospherePress = c.Int(nullable: false),
                        LoggingInterval = c.Int(nullable: false),
                        AltitudeOffset = c.Int(nullable: false),
                        UtcShift = c.Int(nullable: false),
                        AltitudeType = c.String(),
                        AltitudeUnit = c.String(),
                        VerticalSpeedUnit = c.String(),
                        SpeedUnit = c.String(),
                        QnhUnit = c.String(),
                        MultiplemarkerDrop = c.Boolean(nullable: false),
                        DeclarationDigits = c.Int(nullable: false),
                        InstrCalibration = c.Decimal(precision: 18, scale: 2),
                        PressOffset = c.Decimal(precision: 18, scale: 2),
                        Header = c.String(),
                        LoggerSerial = c.String(),
                    })
                .PrimaryKey(t => t.TrackId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longtitude = c.Double(nullable: false),
                        LocationTime = c.DateTime(nullable: false),
                        AltitudePress = c.Int(nullable: false),
                        AltitudeGps = c.Int(nullable: false),
                        VerticalSpeed = c.Int(nullable: false),
                        TlcHdo = c.Int(nullable: false),
                        TlcSiu = c.Int(nullable: false),
                        TrackId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LocationId)
                .ForeignKey("dbo.Tracks", t => t.TrackId, cascadeDelete: true)
                .Index(t => t.TrackId);
            
            CreateTable(
                "dbo.Marks",
                c => new
                    {
                        MarkId = c.Int(nullable: false, identity: true),
                        MarkerId = c.Int(nullable: false),
                        LatitudeMark = c.Double(nullable: false),
                        LongtitudeMark = c.Double(nullable: false),
                        MarkTime = c.DateTime(nullable: false),
                        AltitudePressMark = c.Int(nullable: false),
                        AltitudeGpsMark = c.Int(nullable: false),
                        VerticalSpeedMark = c.Int(nullable: false),
                        TlcHdoMark = c.Int(nullable: false),
                        TlcSiuMark = c.Int(nullable: false),
                        TrackId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MarkId)
                .ForeignKey("dbo.Tracks", t => t.TrackId, cascadeDelete: true)
                .Index(t => t.TrackId);
            
            CreateTable(
                "dbo.PilotLoggerFlights",
                c => new
                    {
                        FlightId = c.Int(nullable: false),
                        PilotId = c.Int(nullable: false),
                        LoggerId = c.Int(nullable: false),
                        Track_TrackId = c.Int(),
                    })
                .PrimaryKey(t => new { t.FlightId, t.PilotId, t.LoggerId })
                .ForeignKey("dbo.Pilots", t => t.PilotId, cascadeDelete: true)
                .ForeignKey("dbo.Flights", t => t.FlightId, cascadeDelete: true)
                .ForeignKey("dbo.Loggers", t => t.LoggerId, cascadeDelete: true)
                .ForeignKey("dbo.Tracks", t => t.Track_TrackId)
                .Index(t => t.FlightId)
                .Index(t => t.PilotId)
                .Index(t => t.LoggerId)
                .Index(t => t.Track_TrackId);
            
            CreateTable(
                "dbo.Flights",
                c => new
                    {
                        FlightId = c.Int(nullable: false, identity: true),
                        FlightName = c.String(),
                        EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FlightId)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        EventName = c.String(),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.PilotEventLoggers",
                c => new
                    {
                        PilotId = c.Int(nullable: false),
                        EventId = c.Int(nullable: false),
                        LoggerId = c.Int(nullable: false),
                        PilotNumber = c.Int(),
                    })
                .PrimaryKey(t => new { t.PilotId, t.EventId, t.LoggerId })
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .ForeignKey("dbo.Loggers", t => t.LoggerId, cascadeDelete: true)
                .ForeignKey("dbo.Pilots", t => t.PilotId, cascadeDelete: true)
                .Index(t => t.PilotId)
                .Index(t => t.EventId)
                .Index(t => t.LoggerId);
            
            CreateTable(
                "dbo.Loggers",
                c => new
                    {
                        LoggerId = c.Int(nullable: false, identity: true),
                        LoggerSerial = c.String(),
                    })
                .PrimaryKey(t => t.LoggerId);
            
            CreateTable(
                "dbo.Pilots",
                c => new
                    {
                        PilotId = c.Int(nullable: false, identity: true),
                        PilotName = c.String(),
                    })
                .PrimaryKey(t => t.PilotId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PilotLoggerFlights", "Track_TrackId", "dbo.Tracks");
            DropForeignKey("dbo.PilotLoggerFlights", "LoggerId", "dbo.Loggers");
            DropForeignKey("dbo.PilotLoggerFlights", "FlightId", "dbo.Flights");
            DropForeignKey("dbo.PilotEventLoggers", "PilotId", "dbo.Pilots");
            DropForeignKey("dbo.PilotLoggerFlights", "PilotId", "dbo.Pilots");
            DropForeignKey("dbo.PilotEventLoggers", "LoggerId", "dbo.Loggers");
            DropForeignKey("dbo.PilotEventLoggers", "EventId", "dbo.Events");
            DropForeignKey("dbo.Flights", "EventId", "dbo.Events");
            DropForeignKey("dbo.Marks", "TrackId", "dbo.Tracks");
            DropForeignKey("dbo.Locations", "TrackId", "dbo.Tracks");
            DropForeignKey("dbo.Declarations", "TrackId", "dbo.Tracks");
            DropIndex("dbo.PilotEventLoggers", new[] { "LoggerId" });
            DropIndex("dbo.PilotEventLoggers", new[] { "EventId" });
            DropIndex("dbo.PilotEventLoggers", new[] { "PilotId" });
            DropIndex("dbo.Flights", new[] { "EventId" });
            DropIndex("dbo.PilotLoggerFlights", new[] { "Track_TrackId" });
            DropIndex("dbo.PilotLoggerFlights", new[] { "LoggerId" });
            DropIndex("dbo.PilotLoggerFlights", new[] { "PilotId" });
            DropIndex("dbo.PilotLoggerFlights", new[] { "FlightId" });
            DropIndex("dbo.Marks", new[] { "TrackId" });
            DropIndex("dbo.Locations", new[] { "TrackId" });
            DropIndex("dbo.Declarations", new[] { "TrackId" });
            DropTable("dbo.Pilots");
            DropTable("dbo.Loggers");
            DropTable("dbo.PilotEventLoggers");
            DropTable("dbo.Events");
            DropTable("dbo.Flights");
            DropTable("dbo.PilotLoggerFlights");
            DropTable("dbo.Marks");
            DropTable("dbo.Locations");
            DropTable("dbo.Tracks");
            DropTable("dbo.Declarations");
        }
    }
}
