namespace IGCDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oneMoreTry : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PilotLoggerFlights", "Track_TrackId", "dbo.Tracks");
            DropIndex("dbo.PilotLoggerFlights", new[] { "Track_TrackId" });
            RenameColumn(table: "dbo.PilotLoggerFlights", name: "Track_TrackId", newName: "TrackId");
            DropPrimaryKey("dbo.PilotLoggerFlights");
            AlterColumn("dbo.PilotLoggerFlights", "TrackId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.PilotLoggerFlights", new[] { "FlightId", "PilotId", "LoggerId", "TrackId" });
            CreateIndex("dbo.PilotLoggerFlights", "TrackId");
            AddForeignKey("dbo.PilotLoggerFlights", "TrackId", "dbo.Tracks", "TrackId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PilotLoggerFlights", "TrackId", "dbo.Tracks");
            DropIndex("dbo.PilotLoggerFlights", new[] { "TrackId" });
            DropPrimaryKey("dbo.PilotLoggerFlights");
            AlterColumn("dbo.PilotLoggerFlights", "TrackId", c => c.Int());
            AddPrimaryKey("dbo.PilotLoggerFlights", new[] { "FlightId", "PilotId", "LoggerId" });
            RenameColumn(table: "dbo.PilotLoggerFlights", name: "TrackId", newName: "Track_TrackId");
            CreateIndex("dbo.PilotLoggerFlights", "Track_TrackId");
            AddForeignKey("dbo.PilotLoggerFlights", "Track_TrackId", "dbo.Tracks", "TrackId");
        }
    }
}
