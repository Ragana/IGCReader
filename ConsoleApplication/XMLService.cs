﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data.Entity;


namespace ConsoleApplication
{
    class XMLService
    {

        //private IGCDataContext _context = new IGCDataContext();
        private PilotService _pilotService = new PilotService();
        private LoggerService _loggerService = new LoggerService();

        //private FlightService _flightService = new FlightService();

        public void SetPilots(string path)
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    string pilotName = node.Attributes["Name"]?.InnerText;
                    Pilot existingPilot = _pilotService.CheckCreatePilot(pilotName);

                }
                _context.SaveChanges();
            }
        }

        public void SetLoggers(string path)
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    string loggerSerial = node.Attributes["LoggerSerial"]?.InnerText;
                    Logger existingLogger = _loggerService.CheckCreateLogger(loggerSerial);

                }
                _context.SaveChanges();
            }
        }

        public void AssignToEventForce(string path, Event thisEvent)//O jei kartojas?
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    string pilotName = node.Attributes["Name"]?.InnerText;
                    int pilotEventId = Int32.Parse(node.Attributes["Number"]?.InnerText);
                    string loggerSerial = node.Attributes["LoggerSerial"]?.InnerText;

                    Pilot existingPilot = _pilotService.CheckCreatePilot(pilotName);
                    Logger existingLogger = _loggerService.CheckCreateLogger(loggerSerial);

                    _context.Pilot.Attach(existingPilot);
                    _context.Logger.Attach(existingLogger);
                    _context.Event.Attach(thisEvent);

                    PilotEventLogger pilotEventLoger = new PilotEventLogger
                    {
                        PilotId = existingPilot.PilotId,
                        LoggerId = existingLogger.LoggerId,
                        EventId = thisEvent.EventId,

                        Pilot = existingPilot,
                        Logger = existingLogger,
                        Event = thisEvent,

                        PilotNumber = pilotEventId
                    };
                    _context.PilotEventLoggers.Add(pilotEventLoger);
                }
                _context.SaveChanges();
            }
        }

        public void AssignToEvent(string path, Event thisEvent)// gal ID?
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    string pilotName = node.Attributes["Name"]?.InnerText;
                    int pilotEventId = Int32.Parse(node.Attributes["Number"]?.InnerText);
                    string loggerSerial = node.Attributes["LoggerSerial"]?.InnerText;

                    Pilot existingPilot = _pilotService.GetPilotByName(pilotName);
                    if (existingPilot == null)
                    {
                        throw new Exception($"Pilotas {pilotName} nerastas");
                    }

                    Logger existingLogger = _loggerService.GetLoggerBySerial(loggerSerial);

                    if (existingLogger == null)
                    {
                        throw new Exception($"Logeris {loggerSerial} nerastas");
                    }

                    _context.Pilot.Attach(existingPilot);
                    _context.Logger.Attach(existingLogger);
                    _context.Event.Attach(thisEvent);

                    PilotEventLogger pilotEventLoger = new PilotEventLogger
                    {
                        PilotId = existingPilot.PilotId,
                        LoggerId = existingLogger.LoggerId,
                        EventId = thisEvent.EventId,

                        Pilot = existingPilot,
                        Logger = existingLogger,
                        Event = thisEvent,

                        PilotNumber = pilotEventId
                    };
                    _context.PilotEventLoggers.Add(pilotEventLoger);
                }
                _context.SaveChanges();
            }
        }

        public void AssignToEventUI(string pilotName, string loggerSerial, int pilotNumber, Event thisEvent)// gal ID?
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                Pilot pilot = _pilotService.GetPilotByName(pilotName);

                Logger logger = _loggerService.GetLoggerBySerial(loggerSerial);

                _context.Pilot.Attach(pilot);
                _context.Logger.Attach(logger);
                _context.Event.Attach(thisEvent);

                PilotEventLogger pilotEventLoger = new PilotEventLogger
                {
                    PilotId = pilot.PilotId,
                    LoggerId = logger.LoggerId,
                    EventId = thisEvent.EventId,

                    Pilot = pilot,
                    Logger = logger,
                    Event = thisEvent,

                    PilotNumber = pilotNumber
                };
                _context.PilotEventLoggers.Add(pilotEventLoger);
                _context.SaveChanges();
            }
        }


    }
}
