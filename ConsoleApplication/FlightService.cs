﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class FlightService
    {
        //private IGCDataContext _context = new IGCDataContext();

        public Flight CreateFlight(string flightName, Event eventThis)
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                Flight existingFlight = _context.Flight.FirstOrDefault(x => x.FlightName == flightName && x.EventId == eventThis.EventId);
                if (existingFlight != null)
                {
                    throw new Exception("Flight already exists");
                }
                else
                {
                    existingFlight = new Flight()
                    {
                        FlightName = flightName,
                        Event = eventThis

                    };
                    _context.Event.Attach(eventThis);
                    _context.Flight.Add(existingFlight);
                    _context.SaveChanges();
                    return existingFlight;
                }
            }
        }

        public Flight GetFlightById(int id)
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                Flight flightToReturn = _context.Flight.Where(x => x.FlightId == id).FirstOrDefault();

                return flightToReturn;
            }
        }
        public List<Flight> GetFlightsbyEvent(Event eventThis)
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                List<Flight> flightsToReturn = _context.Flight.Where(x => x.EventId == eventThis.EventId).ToList();

                return flightsToReturn;
            }
        }
    }
}
