﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using IGCData.Classes;
using IGCData.Model;

namespace ConsoleApplication //TODO apkarpyt declaredgoal pradžias
{
    class Program
    {
        static void Main(string[] args)
        {
            //string _igcPath = @"C:\Users\FlinkeFolkIntern\Downloads\dd\5WR-014.igc";
            string _xmlPath = @"C:\Users\FlinkeFolkIntern\Downloads\dd\Pilots.xml";

            List<string> igcPaths = new List<string>() { @"C:\Users\FlinkeFolkIntern\Downloads\dd\001-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\002-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\003-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\004-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\005-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\006-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\008-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\009-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\010-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\011-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\012-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\013-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\014-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\015-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\016-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\017-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\018-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\019-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\020-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\021-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\022-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\023-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\024-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\025-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\026-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\028-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\029-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\030-0804PM.igc", @"C:\Users\FlinkeFolkIntern\Downloads\dd\031-0804PM.igc" };

            GodMethod(igcPaths, _xmlPath, "Naujas", "Skrydis");
        }


        static void GodMethod(List<string> igcPaths, string _xmlPath, string eventName, string flightName)
        {
            EventService thisService = new EventService();
            Event thisEvent = thisService.CreateEvent(eventName);
            FlightService mewFlightS = new FlightService();
            Flight newFlight = mewFlightS.CreateFlight(flightName, thisEvent);
            XMLService xMLService = new XMLService();
       
            xMLService.AssignToEventForce(_xmlPath, thisEvent);

            foreach (string _igcPath in igcPaths)
            {
                TrackImportService import = new TrackImportService();
                Track track = import.GetTrackInfo(_igcPath);

                import.SetPilotLoggerFlight(thisEvent, newFlight, track);
            }
        }

    }

}



