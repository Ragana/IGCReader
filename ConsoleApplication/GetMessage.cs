﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    public class GetMessage
    {
        private string path;
        private string line;
        private string latitudeS;
        private string longtitudeS;
        private string precisionLat;
        private string precisionLon;
        private string declaredGoalCoordinateDescr;
        private string declaredGoalAltitudeDescr;
        private double latitude;
        private double longtitude;
        private double minutes;
        private double coordinate;
        private int declarationIdReceived;
        private int markerId;
        private int altitudePress;
        private int altitudeGPS;
        private int hdo;
        private int satelitesInUse;
        private int verticalSpeed;
        private char recordType;
        char[] headerIndicator = { 'A', 'H', 'I', 'J', 'L' };
        private DateTime locationTime;
        private Track track;
        StreamReader file;
        StringBuilder buildHeader;
        private List<string> headerList;
        private List<Location> locationRecordsB = new List<Location>();
        private List<Declaration> declarationRecords = new List<Declaration>();
        private List<Mark> markRecords = new List<Mark>();

        public GetMessage(string path)
        {
            this.path = path;
        }
        public void Read()
        {
            headerList = new List<string>();
            track = new Track()
            {
                Locations = new List<Location>(),
                Marks = new List<Mark>(),
                Declarations = new List<Declaration>()
            };
            file = new StreamReader(path);
            buildHeader = new StringBuilder();

            while ((line = file.ReadLine()) != null)
            {
                recordType = line[0];

                if (headerIndicator.Contains(recordType))
                {
                    headerList.Add(line);
                    buildHeader.Append(line);
                }
                if (recordType == 'B')
                {
                    locationRecordsB.Add(RecordTypeB());
                }
                else if (recordType == 'E' && line.Substring(7, 3) == "XX1")
                {
                    declarationRecords.Add(RecordTypeDeclaration());
                }
                else if (recordType == 'E' && line.Substring(7, 3) == "XX0")
                {
                    markRecords.Add(RecordTypeMark());
                }
            }
            TrackHeaderParser header = new TrackHeaderParser(headerList);
            track = header.ParseHeaderInfo();
            track.Header = buildHeader.ToString();

            file.Close();
        }

        Location RecordTypeB()
        {
            locationTime = TimeFromString(line);
            latitudeS = ParseFromString(7, 14, line).Insert(2, ".");
            longtitudeS = ParseFromString(15, 23, line).Insert(3, ".");
            precisionLat = ParseFromString(41, 41, line);
            precisionLon = ParseFromString(42, 42, line);

            latitude = ConvertCoordinate(latitudeS, precisionLat);
            longtitude = ConvertCoordinate(longtitudeS, precisionLon);

            altitudePress = Int32.Parse(ParseFromString(25, 29, line));
            altitudeGPS = Int32.Parse(ParseFromString(30, 34, line));
            hdo = Int32.Parse(ParseFromString(35, 38, line));
            satelitesInUse = Int32.Parse(ParseFromString(39, 40, line));
            verticalSpeed = Int32.Parse(ParseFromString(43, 46, line));

            return new Location()
            {
                Latitude = latitude,
                Longtitude = longtitude,
                LocationTime = locationTime,
                AltitudePress = altitudePress,
                AltitudeGps = altitudeGPS,
                VerticalSpeed = verticalSpeed,
                TlcHdo = hdo,
                TlcSiu = satelitesInUse
            };
        }

        Declaration RecordTypeDeclaration()
        {
            locationTime = TimeFromString(line);
            declarationIdReceived = Int32.Parse(ParseFromString(10, 11, line));
            declaredGoalAltitudeDescr = ParseFromString(12, line.IndexOf(',') - 1, line);
            declaredGoalCoordinateDescr = ParseFromString(line.IndexOf(',') + 1, line.Length - 1, line);
            return new Declaration()
            {
                DeclarationTime = locationTime,
                DeclarationIdReceived = declarationIdReceived,
                DeclaredGoalAltitudeDescr = declaredGoalAltitudeDescr,
                DeclaredGoalCoordinateDescr = declaredGoalCoordinateDescr
            };

        }

        Mark RecordTypeMark()
        {
            locationTime = TimeFromString(line);
            markerId = Int32.Parse(ParseFromString(9, 11, line));
            latitudeS = ParseFromString(12, 19, line).Insert(2, ".");
            longtitudeS = ParseFromString(20, 28, line).Insert(3, ".");
            precisionLat = ParseFromString(46, 46, line);
            precisionLon = ParseFromString(47, 47, line);

            latitude = ConvertCoordinate(latitudeS, precisionLat);
            longtitude = ConvertCoordinate(longtitudeS, precisionLon);

            altitudePress = Int32.Parse(ParseFromString(30, 34, line));
            altitudeGPS = Int32.Parse(ParseFromString(35, 39, line));
            hdo = Int32.Parse(ParseFromString(40, 43, line));
            satelitesInUse = Int32.Parse(ParseFromString(44, 45, line));
            verticalSpeed = Int32.Parse(ParseFromString(48, 51, line));

            return new Mark()
            {
                MarkerId = markerId,
                LatitudeMark = latitude,
                LongtitudeMark = longtitude,
                MarkTime = locationTime,
                AltitudePressMark = altitudePress,
                AltitudeGpsMark = altitudeGPS,
                VerticalSpeedMark = verticalSpeed,
                TlcHdoMark = hdo,
                TlcSiuMark = satelitesInUse,

                //Track = track,
                // TrackId = track.TrackId
            };

        }

        public Track GetTrack()
        {
            return track;
        }

        public List<Location> GetLocations()
        {
            return locationRecordsB;
        }

        public List<Mark> GetMarks()
        {
            return markRecords;
        }

        public List<Declaration> GetDeclaration()
        {
            return declarationRecords;
        }

        private DateTime TimeFromString(string line)
        {
            DateTime locationTime;
            locationTime = DateTime.ParseExact(ParseFromString(1, 6, line), "HHmmss", CultureInfo.InvariantCulture); //TODO add Try
            return locationTime;
        }
        // end needs to be index of last char
        public static string ParseFromString(int start, int end, string line)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = start; i <= end; i++)
            {
                sb.Append(line[i]);
            }
            return sb.ToString();
        }
        private double ConvertCoordinate(string coordinateS, string precision)
        {

            var format = new NumberFormatInfo();
            format.NegativeSign = "-";
            format.NumberDecimalSeparator = ".";

            if (coordinateS.EndsWith("S") || coordinateS.EndsWith("W"))
            {
                coordinateS = coordinateS.Insert(0, "-");
            }
            coordinateS = coordinateS.Remove(coordinateS.Length - 1);
            coordinateS += precision;
            minutes = Double.Parse(ParseFromString(coordinateS.Length - 6, coordinateS.Length - 1, coordinateS));
            minutes = minutes * 100 / 60;
            coordinateS = ParseFromString(0, coordinateS.IndexOf('.'), coordinateS) + minutes.ToString(CultureInfo.InvariantCulture).Replace(".", "");

            coordinate = Double.Parse(coordinateS, format);

            return coordinate;
        }

    }
}




