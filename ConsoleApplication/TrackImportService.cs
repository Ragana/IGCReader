﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    public class TrackImportService
    {
        private Track track;
       
        public Track GetTrackInfo(string path)
        {
            GetMessage getMessage = new GetMessage(path);
            getMessage.Read();
            track = getMessage.GetTrack();
            track.Locations = getMessage.GetLocations();
            track.Marks = getMessage.GetMarks();
            track.Declarations = getMessage.GetDeclaration();
            return track;
        }

        public void SetPilotLoggerFlight(Event eventThis, Flight flight, Track track)
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                PilotEventLogger getEventPilots = _context.PilotEventLoggers.Include(x => x.Pilot).FirstOrDefault(x => x.EventId == eventThis.EventId && x.Logger.LoggerSerial == track.LoggerSerial);


                PilotLoggerFlight newstuff = new PilotLoggerFlight()
                {
                    PilotId = getEventPilots.PilotId,
                    LoggerId = getEventPilots.LoggerId,
                    FlightId = flight.FlightId,
                    Track = track
                };
               //_context.Track.Add(track);
                _context.PilotLoggerFlights.Add(newstuff);
                _context.SaveChanges();
            }
        }


        public void SaveTrackInfoToDB()
        {
            using (IGCDataContext _context = new IGCDataContext())
            {
                
                _context.Track.Add(track);
                //_context.Location.AddRange(locationRecordsB);
                //_context.Mark.AddRange(markRecords);
                //_context.Declaration.AddRange(declarationRecords);

                _context.SaveChanges();
            }
        }




    }
}
