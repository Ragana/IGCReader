﻿using IGCData.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    public class TrackHeaderParser
    {
        string type;
        private char recordType;
        List<string> headers;
        private Track track;
        //Logger logger = new Logger();
        //Pilot pilot = new Pilot();

        public TrackHeaderParser(List<String> headers)
        {
            this.headers = headers;
            track = new Track();
   
        }
        public Track ParseHeaderInfo()
        {
            foreach (string line in headers)
            {
                recordType = line[0];
                if (recordType == 'A')
                {
                    RecordTypeA(line);
                }
                if (recordType == 'H')
                {
                    RecordTypeH(line);
                }
                if (recordType == 'L')
                {
                    RecordTypeL(line);
                }
            }
            return track;
        }
        private void RecordTypeA(string line)
        {
            track.LoggerSerial = line.Substring(4, 3);
        }
        private void RecordTypeH(string line)
        {
            type = line.Substring(1, 4);
            if (type == "FDTE")
            {
                track.TrackDate = DateTime.ParseExact(line.Substring(5, 6), "ddMMyy", CultureInfo.InvariantCulture);
            }
            else if (type == "HFPID")
            {
                //pilot.PilotName = line.Substring(5, 3);
            }
            else if (type == "FDTM")
            {
                track.GeodeticDatum = line.Substring(8, 5);
            }
            else if (type == "FATS")
            {
                track.AtmospherePress = ParseInt(5, line.Length - 1, line);
            }
            else if (type == "FINT")
            {
                track.LoggingInterval = Int32.Parse(line.Substring(5, 3));
            }
            else if (type == "FIER")
            {
                track.AltitudeOffset = ParseInt(5, line.Length - 1, line);
            }
        }

        private void RecordTypeL(string line)
        {
            int afterEqual = line.IndexOf('=') + 1;
            type = GetMessage.ParseFromString(5, line.IndexOf('='), line);

            if (type.Contains("utcshift"))
            {
                track.UtcShift = ParseInt(afterEqual, line.Length - 2, line);
            }
            else if (type.Contains("loggerinterval"))
            {
                track.LoggingInterval = ParseInt(afterEqual, line.Length - 2, line);
            }
            else if (type.Contains("altitude"))
            {
                track.AltitudeType = GetStringAfterEqual(afterEqual, line);
            }
            else if (type.Contains("alt unit"))
            {
                track.AltitudeUnit = GetStringAfterEqual(afterEqual, line);
            }
            else if (type.Contains("vario unit"))
            {
                track.VerticalSpeedUnit = GetStringAfterEqual(afterEqual, line);
            }
            else if (type.Contains("speed unit"))
            {
                track.SpeedUnit = GetStringAfterEqual(afterEqual, line);
            }
            else if (type.Contains("qnh unit"))
            {
                track.QnhUnit = GetStringAfterEqual(afterEqual, line);
            }
            else if (type.Contains("multiple"))
            {
                track.MultiplemarkerDrop = GetStringAfterEqual(afterEqual, line).Contains("yes") ? true : false;
            }
            else if (type.Contains("declaration"))
            {
                track.DeclarationDigits = ParseInt(afterEqual, line.Length - 1, line);
            }
            else if (type.Contains("calibration"))
            {
                track.InstrCalibration = Decimal.Parse(GetMessage.ParseFromString(afterEqual, line.Length - 2, line), CultureInfo.InvariantCulture);
            }
            else if (type.Contains("press. offset"))
            {
                track.PressOffset = Decimal.Parse(GetMessage.ParseFromString(afterEqual, line.Length - 4, line), CultureInfo.InvariantCulture); 
            }
        }

        private int ParseInt(int start, int end, string line)
        {
            return Int32.Parse(GetMessage.ParseFromString(start, end, line));
        }

        private string GetStringAfterEqual(int afterEqual, string line)
        {
            return GetMessage.ParseFromString(afterEqual, line.Length - 1, line);
        }
    }
}
