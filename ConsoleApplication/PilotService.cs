﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class PilotService
    {
        
        public Pilot CreatePilot(string pilotName)
        {
            Pilot existingPilot = GetPilotByName(pilotName);
            if (existingPilot != null)
            {
                throw new Exception("Pilot already exists");
            }
            else
            {
                existingPilot = new Pilot()
                {
                    PilotName = pilotName,

                };
                using (IGCDataContext _context = new IGCDataContext())
                {
                    _context.Pilot.Add(existingPilot);
                    _context.SaveChanges();
                }
                return existingPilot;
            }
        }

        public Pilot GetPilotById(int id)
        {
            Pilot pilotToReturn;
            using (IGCDataContext _context = new IGCDataContext())
            {
                pilotToReturn = _context.Pilot.FirstOrDefault(x => x.PilotId == id);
            }
            return pilotToReturn;

        }

        public Pilot GetPilotByName(string name)
        {
            Pilot pilotToReturn;
            using (IGCDataContext _context = new IGCDataContext())
            {
                pilotToReturn = _context.Pilot
                .FirstOrDefault(x => x.PilotName.ToLower() == name.ToLower());
            }
            return pilotToReturn;
        }

        public Pilot CheckCreatePilot(string pilotName)
        {
            Pilot existingPilot = GetPilotByName(pilotName);

            if (existingPilot == null)
            {
                existingPilot = CreatePilot(pilotName);
            }
            return existingPilot;
        }
    }
}
