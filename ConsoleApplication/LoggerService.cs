﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class LoggerService
    {
        //private IGCDataContext _context = new IGCDataContext();

        public Logger CreateLogger(string loggerSerial)
        {
            Logger existingLogger = GetLoggerBySerial(loggerSerial);
            if (existingLogger != null)
            {
                throw new Exception("Logger already exists");
            }
            else
            {
                existingLogger = new Logger()
                {
                    LoggerSerial= loggerSerial,
                    
                };
                using (IGCDataContext _context = new IGCDataContext())
                {
                    _context.Logger.Add(existingLogger);
                    _context.SaveChanges();
                }
                return existingLogger;
            }
        }

        public Logger GetLoggerById(int id)
        {
            Logger loggerToReturn;
            using (IGCDataContext _context = new IGCDataContext())
            {
                loggerToReturn = _context.Logger.FirstOrDefault(x => x.LoggerId == id);
            }
            return loggerToReturn;

        }

        public Logger GetLoggerBySerial(string serial)
        {
            Logger loggerToReturn;
            using (IGCDataContext _context = new IGCDataContext())
            {
                loggerToReturn = _context.Logger.FirstOrDefault(x => x.LoggerSerial.ToLower() == serial.ToLower());
            }
            return loggerToReturn;
        }

        public Logger CheckCreateLogger(string loggerSerial)
        {
            Logger existingLogger;
            using (IGCDataContext _context = new IGCDataContext())
            {
                existingLogger = GetLoggerBySerial(loggerSerial);
                if (existingLogger == null)
                {
                    existingLogger = CreateLogger(loggerSerial);
                }
            }
            return existingLogger;
        }
    }
}
