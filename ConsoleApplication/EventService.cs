﻿using IGCData.Classes;
using IGCData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ConsoleApplication
{
    class EventService
    {
       // private IGCDataContext _context = new IGCDataContext();

        public Event CreateEvent(string eventName)
        {
           
            Event existingEvent = GetEventByName(eventName);
            if (existingEvent != null)
            {
                throw new Exception("Event already exists");
            }
            else
            {
                existingEvent = new Event()
                {
                    EventName = eventName,
                    Flights = new List<Flight>()
                };
                using (IGCDataContext _context = new IGCDataContext())
                {
                    _context.Event.Add(existingEvent);
                    _context.SaveChanges();
                }
                return existingEvent;
            }
        }

        public Event GetEventById(int id)
        {
            Event eventToReturn;
            using (IGCDataContext _context = new IGCDataContext())
            {
                eventToReturn = _context.Event.FirstOrDefault(x => x.EventId == id);
            }
            return eventToReturn;
        }

        public Event GetEventByName(string name)
        {
            Event eventToReturn;
            using (IGCDataContext _context = new IGCDataContext())
            {
                eventToReturn = _context.Event.FirstOrDefault(x => x.EventName.ToLower() == name.ToLower());
            }
            return eventToReturn;
        }
    }
}
